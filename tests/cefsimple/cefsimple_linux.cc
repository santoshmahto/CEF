// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "tests/cefsimple/simple_app.h"

//#include <X11/Xlib.h>

#include "include/base/cef_logging.h"

#if 0
namespace {

int XErrorHandlerImpl(Display* display, XErrorEvent* event) {
  LOG(WARNING) << "X error received: "
               << "type " << event->type << ", "
               << "serial " << event->serial << ", "
               << "error_code " << static_cast<int>(event->error_code) << ", "
               << "request_code " << static_cast<int>(event->request_code)
               << ", "
               << "minor_code " << static_cast<int>(event->minor_code);
  return 0;
}

int XIOErrorHandlerImpl(Display* display) {
  return 0;
}

}  // namespace
#endif

// Entry point function for all processes.
int main(int argc, char* argv[]) {
  // Provide CEF with command-line arguments.
  CefMainArgs main_args(argc, argv);

  std::string args;
  for (int i = 0; i < argc; ++i) {
    args += argv[i];
    args += " ";
  }
  fprintf(stderr, "Executing SimpleApp: %s\n", args.c_str());

  // SimpleApp implements application-level callbacks for the browser process.
  // It will create the first browser instance in OnContextInitialized() after
  // CEF has initialized.
  CefRefPtr<SimpleApp> app(new SimpleApp);

  // CEF applications have multiple sub-processes (render, plugin, GPU, etc)
  // that share the same executable. This function checks the command-line and,
  // if this is a sub-process, executes the appropriate logic.
  int exit_code = CefExecuteProcess(main_args, app.get(), NULL);
  if (exit_code >= 0) {
    // The sub-process has completed so return here.
    fprintf(stderr, "Early quitting SimpleApp: %s exit_code=%d\n", args.c_str(), exit_code);
    return exit_code;
  }

#if 0
  // Install xlib error handlers so that the application won't be terminated
  // on non-fatal errors.
  XSetErrorHandler(XErrorHandlerImpl);
  XSetIOErrorHandler(XIOErrorHandlerImpl);
#endif

  // Specify CEF global settings here.
  CefSettings settings;

  fprintf(stderr, "Creating SimpleApp: %s exit_code=%d\n", args.c_str(), exit_code);

  fprintf(stderr, "Initializing Cef for SimpleApp: %s exit_code=%d\n", args.c_str(), exit_code);
  // Initialize CEF for the browser process.
  CefInitialize(main_args, settings, app.get(), NULL);

  fprintf(stderr, "Entering mainloop for SimpleApp: %s\n", args.c_str());
  // Run the CEF message loop. This will block until CefQuitMessageLoop() is
  // called.
  CefRunMessageLoop();

  // Shut down CEF.
  CefShutdown();

  fprintf(stderr, "Quitting SimpleApp: %s\n", args.c_str());

  return 0;
}
