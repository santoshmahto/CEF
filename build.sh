#!/bin/sh
export CEF_USE_GN=1
export PATH=$SRC_DIR/depot_tools:$PATH

if ! [ -d ../src ] && ! [ -d cef/libcef ]; then
  echo "Must be run from inside chromium/src"
fi

GN_DEFINES=""
GN_DEFINES=$GN_DEFINES" treat_warnings_as_errors=false"
GN_DEFINES=$GN_DEFINES" ozone_platform_wayland=true"
GN_DEFINES=$GN_DEFINES" ozone_platform=wayland"
GN_DEFINES=$GN_DEFINES" use_ozone=true"
GN_DEFINES=$GN_DEFINES" use_x11=false"
GN_DEFINES=$GN_DEFINES" use_aura=true"
GN_DEFINES=$GN_DEFINES" enable_nacl=false"
GN_DEFINES=$GN_DEFINES" remove_webcore_debug_symbols=true"
GN_DEFINES=$GN_DEFINES" ozone_auto_platforms=false"
GN_DEFINES=$GN_DEFINES" enable_package_mash_services=true"
GN_DEFINES=$GN_DEFINES" dcheck_always_on=false"
GN_DEFINES=$GN_DEFINES" use_xkbcommon=true"
export GN_DEFINES

cd cef
./cef_create_projects.sh

cd ..
ninja -C out/Release_GN_x64/ $@ chrome cefsimple chrome_sandbox

